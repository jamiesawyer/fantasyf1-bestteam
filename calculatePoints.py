__author__ = 'Jamie Sawyer'
import itertools

SCORES = [25, 18, 15, 12, 10, 8, 6, 4, 2, 1]
TEAMS = {'MER': ['HAM', 'ROS'],
         'FER': ['VET', 'RAI'],
         'MCL': ['BUT', 'ALO'],
         'RBR': ['RIC', 'KVY'],
         'WIL': ['MAS', 'BOT'],
         'FOR': ['PER', 'HUL'],
         'LOT': ['GRO', 'MAL'],
         'SAU': ['ERI', 'NAS'],
         'STR': ['VER', 'SAI'],
         'MAN': ['STE', 'MRH']}
DRIVERS = ['BUT', 'ALO', 'VET', 'RAI', 'GRO', 'MAL', 'PER', 'HUL', 'HAM', 'ROS', 'VER', 'SAI', 'MAS', 'BOT', 'RIC', 'KVY', 'ERI', 'NAS', 'STE', 'MRH']
DRIVER_COSTS = {'BUT': 17, 'ALO': 18, 'VET': 18, 'RAI': 15, 'GRO': 9, 'MAL': 6, 'PER': 9, 'HUL': 9, 'HAM': 22, 'ROS': 20, 'VER': 4, 'SAI': 5, 'MAS': 13, 'BOT': 15, 'RIC': 17, 'KVY': 11, 'ERI': 3, 'NAS': 3, 'STE': 3, 'MRH': 3}
TEAM_COSTS = {'MER': 23, 'RBR': 20, 'WIL': 17, 'FER': 14, 'MCL': 14, 'FOR': 12, 'LOT': 7, 'STR': 7, 'SAU': 4, 'MAN': 2}

def race_points(driver, result):
    position = result['race'].index(driver)
    if position < 10:
        return SCORES[position]
    else:
        return 0


def quali_points(driver, result):
    for t, d in TEAMS.iteritems():
        if driver in d:
            teammate = d[1-(d.index(driver))]
    if result['quali'].index(driver) < result['quali'].index(teammate):
        return 5
    else:
        return 0


def bonus_points(driver, result):
    positions = result['quali'].index(driver) - result['race'].index(driver)
    if positions > 0:
        return 3*positions
    else:
        return 0


def team_points(team, result):
    score = 0
    for driver in TEAMS[team]:
        score += race_points(driver, result)
    return score


def driver_points(driver, result):
    return race_points(driver, result) + quali_points(driver, result) + bonus_points(driver, result)


def score_per_driver(result):
    for d in DRIVERS:
        print d
        print driver_points(d, result)


def create_order():
    order = []
    position = 1
    while len(order) < 20:
        print '\n\n\n====REMAINING DRIVERS====\n'
        for d in DRIVERS:
            if d not in order:
                print d
        fail = True
        while fail:
            choice = raw_input('Which Driver comes in position %d' % position)
            fail = choice not in DRIVERS or choice in order
        order += [choice]
        position += 1
    return order


def create_result():
    print 'Qualification!'
    quali = create_order()
    print 'Race!'
    race = create_order()
    return {'quali': quali, 'race': race}


def get_best_team(result):
    temp_teams = itertools.permutations(TEAMS,3)
    temp_drivers = itertools.permutations(DRIVERS,3)
    teams = []
    drivers = []
    for t in temp_teams:
        price = sum([TEAM_COSTS[x] for x in t])
        if price < 67:
            score = sum([team_points(x, result) for x in t])
            teams.append((t, price, score))
    for d in temp_drivers:
        price = sum([DRIVER_COSTS[x] for x in d])
        if price < 63:
            score = sum([driver_points(x, result) for x in d])
            drivers.append((d, price, score))
    full_combos = []
    for dc in drivers:
        for tc in teams:
            if dc[1]+tc[1] < 76:
                full_combos.append((dc[0],tc[0],dc[1]+tc[1],dc[2]+tc[2]))
    max = 0
    for k in full_combos:
        if k[3] > max:
            best = k
            max = k[3]
    print best




re = create_result()
get_best_team(re)